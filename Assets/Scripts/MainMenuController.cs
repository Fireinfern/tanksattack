using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.Netcode;
using UnityEngine;
using Unity.Netcode.Transports.UTP;

public class MainMenuController : MonoBehaviour
{
    
    
    [SerializeField] private TMP_InputField ipText;

    [SerializeField] private TMP_InputField portText;

    [SerializeField] private string defaultIp = "202.81.232.7";

    [SerializeField] private string defaultPort = "993";
    
    private void Start()
    {
        ipText.text = defaultIp;
        portText.text = defaultPort;
    }

    public void ConnectToServer()
    {
        UnityTransport transport = (UnityTransport) NetworkManager.Singleton.NetworkConfig.NetworkTransport;
        transport.SetConnectionData(ipText.text, ushort.Parse(portText.text));
        NetworkManager.Singleton.StartClient();
        NetworkManager.Singleton.OnClientConnectedCallback += ClientConnected;
    }

    void ClientConnected(ulong id)
    {
        GetComponent<Canvas>().enabled = false;
    }

    public void StartHost()
    {
        UnityTransport transport = (UnityTransport) NetworkManager.Singleton.NetworkConfig.NetworkTransport;
        transport.SetConnectionData("127.0.0.1", ushort.Parse(portText.text), "0.0.0.0");
        NetworkManager.Singleton.StartHost();
        GetComponent<Canvas>().enabled = false;
    }

    public void StartServer()
    {
        UnityTransport transport = (UnityTransport) NetworkManager.Singleton.NetworkConfig.NetworkTransport;
        transport.SetConnectionData("127.0.0.1", ushort.Parse(portText.text), "0.0.0.0");
        NetworkManager.Singleton.StartServer();
        GetComponent<Canvas>().enabled = false;
    }

}
