using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Unity.Netcode; // Include the Netcode namespace

namespace Networking
{
    public class HealthManager : NetworkBehaviour // Inherit from NetworkBehaviour
    {
        private NetworkVariable<int> _networkHealth = new NetworkVariable<int>(10);
        public int health = 10; // Starting health
        private Text healthText; // Reference to the Text UI
        private SoundManager soundManager;
        private TankMovement _tankMovement;

        void Start()
        {
            _tankMovement = GetComponent<TankMovement>();
            soundManager = GetComponent<SoundManager>();
            if (IsServer)
            {
                _networkHealth.Value = health;
            }
            // Use IsLocalPlayer from NetworkBehaviour
            if (IsLocalPlayer)
            {
                // Assuming the healthText is a child of this GameObject or under a specific player UI Canvas
                healthText = GetComponentInChildren<Text>();

                if (healthText == null)
                {
                    Debug.LogError("Health text not found!");
                    return;
                }

                if (soundManager == null)
                {
                    Debug.LogError("SoundManager not found!");
                    return;
                }
                
                if (_tankMovement == null)
                {
                    Debug.LogError("TankMovement not found!");
                    return;
                }

                _networkHealth.OnValueChanged += OnHealthValueChanged;
                
                UpdateHealthText();
                InvokeRepeating("IncreaseHealth", 5f, 5f); // Repeats every 5 seconds after a 5-second delay
            }
            
        }

        private void OnHealthValueChanged(int oldValue, int newValue)
        {
            UpdateHealthText();
        }

        void IncreaseHealth()
        {
            if (IsServer)
            {
                if (_networkHealth.Value < health)
                {
                    _networkHealth.Value += 1;
                }
                UpdateHealthText();
            }
        }

        public void DecreaseHealth(int amount)
        {
            if (IsServer)
            {
                _networkHealth.Value -= amount;
                UpdateHealthText();
                // Call the ClientRpc to play the collision sound on all clients
                PlayCollisionSoundClientRpc();
                
                if (_networkHealth.Value <= 0)
                {
                    _tankMovement.KillPlayerClientRpc();
                    _networkHealth.Value = health;
                }
            }
        }

        [ClientRpc]
        private void PlayCollisionSoundClientRpc()
        {
            soundManager.PlayCollisionSound();
        }
        void UpdateHealthText()
        {
            if (healthText && IsLocalPlayer)
            {
                healthText.text = "Health: " + _networkHealth.Value.ToString();
            }
        }

        // Decrease health on physics-based collisions
        void OnCollisionEnter(Collision collision)
        {
            if (IsServer)
            {
                if (collision.gameObject.CompareTag("Bullet")) {
                    DecreaseHealth(1);
                }
            }
        }

        // Decrease health on trigger-based collisions
        void OnTriggerEnter(Collider other)
        {
            if (IsServer)
            {
                DecreaseHealth(1);
            }
        }
    }
}
