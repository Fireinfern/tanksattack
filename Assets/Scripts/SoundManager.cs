using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Unity.Netcode; // Include the Netcode namespace


public class SoundManager : NetworkBehaviour
{
    public AudioSource collisionSound;
    public AudioSource triggerSound;
    public AudioSource rotationSound;
    public AudioSource movementSound;
    public AudioSource backgroundSound;

    public AudioSource tankStartsRotation;
    public AudioSource tankStopsRotation;
    public AudioSource tankRotating;
    
    void Start()
    {
        // Assuming this script is attached to the player prefab
        if (IsLocalPlayer) // Check if this is the local player
        {
            PlayBackgroundSound();
        }
    }

    private void PlayBackgroundSound()
    {
        backgroundSound.loop = true;
        backgroundSound.Play();
    }

    public void PlayCollisionSound()
    {
        collisionSound.Play();
    }

    public void PlayTriggerSound()
    {
        triggerSound.Play();
    }

    public void PlayRotationSound()
    {
        if (!rotationSound.isPlaying)
        {
            rotationSound.Play();
        }
    }

    public void PlayStartRotationSound()
    {
        if (!tankStartsRotation.isPlaying)
        {
            tankStartsRotation.Play();
        }
    }

    public void PlayStopRotationSound()
    {
        if (!tankStopsRotation.isPlaying)
        {
            tankStopsRotation.Play();
        }
    }

    public void PlayRotatingSound()
    {
        if (!tankRotating.isPlaying)
        {
            tankRotating.Play();
        }
    }

    public void StopRotationSound()
    {
        if (rotationSound.isPlaying)
        {
            rotationSound.Stop();
        }
    }

    public void StopRotatingSound()
    {
        if (tankRotating.isPlaying)
        {
            tankRotating.Stop();
        }
    }

    public void PlayMovementSound()
    {
        if (!movementSound.isPlaying)
        {
            movementSound.Play();
        }
    }

    public void StopMovementSound()
    {
        if (movementSound.isPlaying)
        {
            movementSound.Stop();
        }
    }
}
