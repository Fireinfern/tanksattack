using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class MoveProjectile : NetworkBehaviour
{
    [SerializeField] private float shootForce = 500;
    [SerializeField] private float liveTime = 10f;
    private Rigidbody rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.AddForce(transform.forward * shootForce, ForceMode.Force);
        if (IsClient && !IsHost) return;
        StartCoroutine(Lifetime());
    }

    private void OnServerInitialized()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator Lifetime()
    {
        yield return new WaitForSeconds(liveTime);
        DestroyBullet();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (IsClient && !IsHost) return;
        NetworkObject otherNetworkObject = other.GetComponent<NetworkObject>();
        if (otherNetworkObject && otherNetworkObject.OwnerClientId == OwnerClientId) return;
        DestroyBullet();
    }
    
    private void DestroyBullet()
    {
        NetworkObject.Despawn(true);
        StopCoroutine(Lifetime());
        Destroy(gameObject);
    }
}
