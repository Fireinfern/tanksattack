﻿using System;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.Serialization;

namespace Networking
{
    public class ClientCamera : NetworkBehaviour
    {
        [SerializeField] private Camera playerCamera;
        
        private void Start()
        {
            if (IsLocalPlayer)
            {
                playerCamera.gameObject.SetActive(true);
                playerCamera.enabled = true;
            }
            else
            {
                playerCamera.gameObject.SetActive(false);
                playerCamera.enabled = false;
            }
        }
    }
}