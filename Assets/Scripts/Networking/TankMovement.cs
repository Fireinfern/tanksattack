﻿using System;
using System.Collections;
using Unity.Netcode;
using Unity.Netcode.Components;
using UnityEngine;

namespace Networking
{
    public class TankMovement : NetworkBehaviour
    {
        private Rigidbody _rigidbody;
        private SoundManager soundManager;

        [SerializeField]
        private float movementSpeed = 20.0f;

        [SerializeField]
        private float rotationRate = 10.0f;

        private void Start()
        {
            _rigidbody = GetComponent<Rigidbody>();
            soundManager = GetComponent<SoundManager>();
        }

        private void Update()
        {
            // hate this code XD
            Vector3 position = Vector3.zero;
            float speed = (Time.deltaTime * movementSpeed);
            if (Input.GetKey(KeyCode.W))
            {
                position += transform.forward * speed;
                soundManager.PlayMovementSound();
            }
            else if (Input.GetKey(KeyCode.S))
            {
                position -= transform.forward * speed;
                soundManager.PlayMovementSound();
            }
            else
            {
                soundManager.StopMovementSound();
            }

            position += transform.position;
            _rigidbody.MovePosition(position);

            float rotationAngle = 0.0f;
            if (Input.GetKey(KeyCode.A))
            {
                rotationAngle -= rotationRate * Time.deltaTime;
                if (soundManager.tankRotating.isPlaying == false)
                {
                    soundManager.PlayRotatingSound();
                }
            }
            else if (Input.GetKey(KeyCode.D))
            {
                rotationAngle += rotationRate * Time.deltaTime;
                if (soundManager.tankRotating.isPlaying == false)
                {
                    soundManager.PlayStartRotationSound();
                    soundManager.PlayRotatingSound();
                }
            }
            else
            {
                if (soundManager.tankRotating.isPlaying == true) {
                    soundManager.StopRotatingSound();
                    soundManager.PlayStopRotationSound();
                }
            }

            transform.Rotate(Vector3.up, rotationAngle);
        }

        [ClientRpc]
        public void KillPlayerClientRpc()
        {
            transform.position = Vector3.zero;
        }
    }
}
