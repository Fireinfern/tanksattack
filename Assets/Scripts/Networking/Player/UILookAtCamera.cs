using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UILookAtCamera : MonoBehaviour
{
    private void LateUpdate()
    {
        if (Camera.allCamerasCount <= 1) return;
        var camera = Camera.allCameras[1];
        if (camera == null) return;
        transform.LookAt(transform.position + camera.transform.forward);
    }
}
