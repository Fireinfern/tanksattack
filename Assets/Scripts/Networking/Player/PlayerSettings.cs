using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.Collections;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.Serialization;

public class PlayerSettings : NetworkBehaviour
{
    [SerializeField] private MeshRenderer[] _meshRendererList;
    [SerializeField] private TextMeshProUGUI playerName;

    //private NetworkVariable<FixedString128Bytes> _networkPlayerName =
    //    new NetworkVariable<FixedString128Bytes>("Player: 0", NetworkVariableReadPermission.Everyone,
    //        NetworkVariableWritePermission.Server);

    public List<Color> colors = new List<Color>();
    private void Awake()
    {
        _meshRendererList = GetComponentsInChildren<MeshRenderer>();
    }

    public override void OnNetworkSpawn()
    {
        var cId = (int)OwnerClientId;
        playerName.text = "Player: " + (cId + 1);
        var index = (cId % _meshRendererList.Length);
        foreach (var meshRenderer in _meshRendererList)
        {
            meshRenderer.material.color = colors[index];
        }
    }
}
