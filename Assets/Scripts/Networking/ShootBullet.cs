using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class ShootBullet : NetworkBehaviour
{
    [SerializeField]
    private GameObject bullet;

    [SerializeField]
    private Transform shootTransform;

    private Rigidbody _playerRigidbody;
    private SoundManager soundManager;

    [SerializeField]
    private float fireRate = 1.0f;

    private float cooldownTimer = 0.0f;

    private void Start()
    {
        // Get the SoundManager component from the same GameObject
        soundManager = GetComponent<SoundManager>();
    }

    private void Update()
    {
        if (!IsOwner)
            return;
        if (cooldownTimer > 0.0f)
        {
            cooldownTimer -= Time.deltaTime;
            return;
        }
        if (!Input.GetKeyDown(KeyCode.Mouse0))
        {
            return;
        }
        cooldownTimer = fireRate;
        ShootServerRpc();
    }

    private void OnServerInitialized()
    {
        _playerRigidbody = GetComponent<Rigidbody>();
    }

    [ServerRpc]
    private void ShootServerRpc()
    {
        var go = Instantiate(bullet, shootTransform.position, shootTransform.rotation);
        go.GetComponent<NetworkObject>().Spawn();
        if (_playerRigidbody)
        {
            _playerRigidbody.AddForce(transform.forward * -200.0f, ForceMode.Impulse);
        }
        // Call the Client RPC to play the sound
        PlayShootSoundClientRpc();
    }

    [ClientRpc]
    private void PlayShootSoundClientRpc()
    {
        soundManager.PlayTriggerSound();
    }
}
